Categories:Time
License:GPLv3
Web Site:https://github.com/ligi/SCR/blob/HEAD/README.md
Source Code:https://github.com/ligi/SCR
Issue Tracker:https://github.com/ligi/SCR/issues

Auto Name:32c3 SCR
Summary:Resolve Schedule conflicts
Description:
Mark the talks you want to see at 32C3 and prevent schedule conflicts this way.
.

Repo Type:git
Repo:https://github.com/ligi/SCR.git

Build:0.7,7
    commit=0.7
    subdir=app
    gradle=yes
    prebuild=sed -i '/play_services/d' build.gradle && \
        sed -i '/android-sdk-manager/d' build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.7
Current Version Code:7
