Categories:Multimedia
License:GPLv3+
Web Site:https://github.com/gateship-one/odyssey/blob/HEAD/README.md
Source Code:https://github.com/gateship-one/odyssey
Issue Tracker:https://github.com/gateship-one/odyssey/issues

Auto Name:Odyssey
Summary:Listen to music files
Description:
Music player that is optimized for speed (even with large music libraries). On
the other hand the players is designed with the Material Design Guidelines in
mind and we try to follow them as close as possible.

It's main features are a fast music library (artist, album, file browser).

A basic playlist management functionality is also part of this player.

To be able to play audio books and podcast this player has a bookmark feature,
that allows you to save your playlist and the playback position, to resume your
audio book/podcast later.

For more comfort you are able to use the launcher widget to have quick control
over your music playback.
.

Repo Type:git
Repo:https://github.com/gateship-one/odyssey

Build:1.0,6
    commit=release-6
    subdir=app
    gradle=yes

Build:1.0,7
    commit=release-7
    subdir=app
    gradle=yes

Build:1.0.8,8
    commit=release-8
    subdir=app
    gradle=yes

Build:1.0.9,9
    commit=release-9
    subdir=app
    gradle=yes

Auto Update Mode:Version release-%c
Update Check Mode:Tags
Current Version:1.0.9
Current Version Code:9
