Categories:Multimedia
License:GPLv3+
Web Site:https://github.com/fr3ts0n/StageFever/blob/HEAD/README.md
Source Code:https://github.com/fr3ts0n/StageFever
Issue Tracker:https://github.com/fr3ts0n/StageFever/issues

Auto Name:StageFever
Summary:Display stage notes, AMP/effect settings etc
Description:
Shows stage notes, AMP/effect settings, BPM etc. for any song in setlist. A
setlist is a csv file following the scheme:

Song;Artist;Settings;BPM;Stage notes
.

Repo Type:git
Repo:https://github.com/fr3ts0n/StageFever.git

Build:1.0,1
    commit=2679d1cb66ad2d298c5b0f64e4b336087bd5ebb0

Build:1.0.1,10001
    commit=a3f15c78cab215620a1b0d18ef34997c3f488d65

Build:1.0.3,10003
    commit=V1.0.3

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0.3
Current Version Code:10003
